#include <Arduino.h>
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
/* Definiciones manejo motores */
#define INA1 26 //izquierda 1
#define INA2 27 // izquierda 2
#define INA3 33 // derecha 1
#define INA4 25 // derecha 2

#define PWMA_Ch    0  // Derecha
#define PWMA_Res   8  //resolucion del pwm
#define PWMA_Freq  60
   
#define PWMB_Ch    1  // Izquierda
#define PWMB_Res   8  //resolucion del pwm
#define PWMB_Freq  60

/* Pines para manejo de encoders */
#define ENC_IZQ 21
#define ENC_DER 19

/*Tiempo de muestreo*/
#define TSAMPLE 100


/* Definicion de variables Globales */
uint32_t u32gTicsACT_L = 0;
uint32_t u32gTicsACT_R = 0;


//Variables Distancia Lineal/

float dm = 6.7;
int L = 14; 
int n = 20;

int disTickDer = 0;
int disTickIzq = 0;

double Dr=0;
double Dl=0;
float Dc=0;

float x=0;
float y=0;
float pi=0;
float pi_d =0;
float Kp = 50;

unsigned long tAct = 0;
unsigned long tAnt = 0;
unsigned long tActTL = 0;
unsigned long tActTR = 0;

int paso = 1;
int pwmL = 0;
int pwmR = 0;


void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32t"); //Bluetooth device name
  
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  pinMode(2, OUTPUT);
  pinMode(INA1, OUTPUT);//salidas esp32
  pinMode(INA2, OUTPUT);
  pinMode(INA3, OUTPUT);
  pinMode(INA4, OUTPUT);
  pinMode(ENC_IZQ,INPUT);//entradas esp32
  pinMode(ENC_DER,INPUT);

  delay(4000);
  /* Interrupciones para lectura de encoder */  
  attachInterrupt(digitalPinToInterrupt(ENC_IZQ), isrEncoderIzq, RISING);
  attachInterrupt(digitalPinToInterrupt(ENC_DER), isrEncoderDer, RISING);
  
  /*Test de un motor */
  digitalWrite(INA2, LOW);
  digitalWrite(INA3, LOW);
  
  ledcAttachPin(INA1, PWMA_Ch);     //configuramos el pwm del motor izquierdo
  ledcSetup(PWMA_Ch, PWMA_Freq, PWMA_Res);
  ledcAttachPin(INA4, PWMB_Ch);     //configuramos el pwm del motor derecho
  ledcSetup(PWMB_Ch, PWMB_Freq, PWMB_Res);

  pwmL = 140;
  pwmR = 146;
  
}
void loop() {

  digitalWrite(2, HIGH);
  ledcWrite(PWMA_Ch, pwmL); // derecha 
  ledcWrite(PWMB_Ch, pwmR); // izquierda 
  
  
  if (x <= 100 && paso == 1) {    //primer linea del cuadrado angulo = 0° 
    mover();
  }
  if (x >= 100 && paso == 1) {
    pi_d = (3.141592 / 2.0);      //cuando termine de trazar x=100, angulo 90°
    paso++;                       //vamos a paso 2
  }
  
  if (y <= 100 && paso == 2) {    //segunda traza o line del cuadrado angulo = 90°
    mover();
  }
  if (y >= 100 && paso == 2) {    
    pi_d = 3.141592;              //cuando termine de trazar y = 100 se asigna angulo = 180° o igual a pi
    paso++;                       //vamos a paso 3
  }
  
  if (x >= 0 && paso == 3) {      //tercera traza o line del cuadrado angulo = 180°
    mover();
  }
  if (x <= 0 && paso == 3) {
    pi_d = 3*(3.141592 / 2.0);    //cuando termine de trazar x : 100 => 0 se asigna angulo = 270° o igual a 3*pi/2
    paso++;                       //vamos a paso 4
  }
  
  if (y >= 0 && paso == 4) {      //cuarta traza o linea del cuadrado angulo = 270°
    mover();
  }
  if (y <= 0 && paso == 4) {      
    pwmL=0;                       //cuando finalize la traza se detiene el carro para finalizar
    pwmR=0;
    pi_d = 0;
    paso++;                       //siguiente paso que es finalizar
  }
}

void mover (void){
  tAct = millis();
    if((tAct - tAnt) > TSAMPLE) //toma muestra despues de Tsample milisegundos
    {
      odometria();
      Serial.print("Posicion: ");
      Serial.print(x);
      Serial.print(" ; ");
      Serial.print(y);
      Serial.print(" ; ");
      Serial.println(pi);
      Dc = 0;
      tAnt = tAct;
      Serial.println("DistDer: " + String(Dr));
      Serial.println("DistIzq: " + String(Dl));
      
      if(Kp*(pi - pi_d) < 0){  //comparacion del error amplificado por Kp
        pwmR=120;
        pwmL=93;
      }
      if(Kp*(pi - pi_d) > 0){
        pwmL=125;
        pwmR=93;
      }
      if(Kp*(pi - pi_d) < -10){  //error amplificado por Kp
        pwmR=120;
        pwmL=20;
      }
      if(Kp*(pi - pi_d) > 10){
        pwmL=125;
        pwmR=20;
      }
      
      u32gTicsACT_R = 0;
      u32gTicsACT_L = 0;
      
      SerialBT.print(x);
      SerialBT.print(";");
      SerialBT.print(y);
      SerialBT.print(";");
      SerialBT.println(pi);
 
      
    }
  } 
  
    

 /************************
 *  Interrupcion que se genera cada flanco de subida del encoder
 *  Ntick = = una vuelta del encoder. 
 * 
 ***********************/
void isrEncoderIzq(void ){  //interrupcion del encoder izquierdo 
  if((millis()-tActTL)>14){
  u32gTicsACT_L++;
  tActTL=millis();

  }
  
  
}

void isrEncoderDer(void ){  //interrupcion del encoder derecho
  if((millis()-tActTR)>14){
    u32gTicsACT_R++;
    tActTR=millis();

  }
}

//odometria/
void odometria(void){
  //dTickR = tRacT - tRanT; 
  disTickDer = (int) u32gTicsACT_R;
  Dr =  3.1416 * dm * ((double)disTickDer/(double)n);
  
  //dTickL = tLacT - tLanT;
  disTickIzq = (int) u32gTicsACT_L; 
  Dl =  3.1416 * dm * ((double)disTickIzq/(double)n);
  
  Dc = (Dr + Dl)/2;
  x = x + Dc*cos(pi);             // posición del punto X actual
  y = y + Dc*sin(pi);             // posición del punto Y actual
  pi = pi + ((Dr - Dl)/L);        // posición Angular actua-l  
  
}
