#include <Arduino.h>
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
/* Definiciones manejo motores */
#define INA1 26 //izquierda 1
#define INA2 27 // izquierda 2
#define INA3 33 // derecha 1
#define INA4 25 // derecha 2

#define PWMA_Ch    0  //resolucion del pwm
#define PWMA_Res   8
#define PWMA_Freq  60
 
#define PWMB_Ch    1
#define PWMB_Res   8  //resolucion del pwm
#define PWMB_Freq  60

/* Pines para manejo de encoders */
#define ENC_IZQ 21
#define ENC_DER 19


/* Constante nro de orificios encoder */

#define TSAMPLE 100


/* Definicion de variables Globales */

uint32_t u32gTActual = 0;
uint32_t u32gTAnterior = 0;
uint32_t u32gTicsACT_L = 0;
uint32_t u32gTicsACT_R = 0;

//Variables Distancia Lineal/

float dm = 6.7;
int L = 14; 
int n = 20;

int disTickDer = 0;

int disTickIzq = 0;


double Dr=0;
double Dl=0;
float Dc=0;
//float DcAnt=0;

float x=0;
float y=0;
float pi=0;
float pi_d =0;
float Kp = 50;

unsigned long tAct = 0;
unsigned long tAnt = 0;
unsigned long tActTL = 0;
unsigned long tActTR = 0;

int pwmL = 0;
int pwmR = 0;

int giroIzq=0,giroDer=0;

void odometria (void);

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32t"); //Bluetooth device name
  
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  pinMode(2, OUTPUT);
  pinMode(INA1, OUTPUT);
  pinMode(INA2, OUTPUT);
  pinMode(INA3, OUTPUT);
  pinMode(INA4, OUTPUT);
  pinMode(ENC_IZQ,INPUT);
  pinMode(ENC_DER,INPUT);

  delay(3000);
  /* Interrupciones para lectura de encoder */  
  attachInterrupt(digitalPinToInterrupt(ENC_IZQ), isrEncoderIzq, RISING);
  attachInterrupt(digitalPinToInterrupt(ENC_DER), isrEncoderDer, RISING);
  
  /*Test de un motor */
  digitalWrite(INA2, LOW);
  digitalWrite(INA3, LOW);
  
  ledcAttachPin(INA1, PWMA_Ch);     //configuramos el pwm del motor izquierdo
  ledcSetup(PWMA_Ch, PWMA_Freq, PWMA_Res);
  ledcAttachPin(INA4, PWMB_Ch);     //configuramos el pwm del motor derecho
  ledcSetup(PWMB_Ch, PWMB_Freq, PWMB_Res);

  
  pwmL = 140;
  pwmR = 146;
  
}
void loop() {
  
  ledcWrite(PWMA_Ch, pwmL); // Izquierda 
  ledcWrite(PWMB_Ch, pwmR); // Derecha 
  
  if (x <= 100) {
    
    tAct = millis();
    if((tAct - tAnt) > TSAMPLE) //toma muestra despues de Tsample milisegundos
    {
      odometria();
      Serial.print("Posicion: ");
      Serial.print(x);
      Serial.print(" ; ");
      Serial.print(y);
      Serial.print(" ; ");
      Serial.println(pi);
      Dc = 0;
      tAnt = tAct;

      SerialBT.print(x);
      SerialBT.print(";");
      SerialBT.print(y);
      SerialBT.print(";");
      SerialBT.println(pi);
  
      if((Kp*pi) < pi_d){  //error amplificado por Kp
        pwmR=120;
        pwmL=93;
      }
      if((Kp*pi) > pi_d){
        pwmL=125;
        pwmR=93;
      }
      u32gTicsACT_R = 0;//variables anti rebote
      u32gTicsACT_L = 0;
    }
  }
  else{
    pwmR=0;    // derecha
    pwmL=0;    // izquierda
    
    }  
  
 }
    
 /************************
 *  Interrupcion que se genera cada flanco de subida del encoder
 *  Ntick = = una vuelta del encoder. 
 * 
 ***********************/
void isrEncoderIzq(void ){
  if((millis()-tActTL)>13){
  u32gTicsACT_L++;
  tActTL=millis();

  }
}

void isrEncoderDer(void ){
  if((millis()-tActTR)>13){
    u32gTicsACT_R++;
    tActTR=millis();

  }
}

//odometria/
void odometria(void){
  disTickDer = (int) u32gTicsACT_R;
  Dr =  3.1416 * dm * ((double)disTickDer/(double)n);
  
  disTickIzq = (int) u32gTicsACT_L; 
  Dl =  3.1416 * dm * ((double)disTickIzq/(double)n);
  
  Dc = (Dr + Dl)/2;
  x = x + Dc*cos(pi);             // posición del punto X actual
  y = y + Dc*sin(pi);             // posición del punto Y actual
  pi = pi + ((Dr - Dl)/L);        // posición Angular actua-l  

  
     
}
