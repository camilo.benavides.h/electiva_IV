#define INA1 26 //izquierda 1
#define INA2 27 // izquierda 2
#define INA3 33 // derecha 1
#define INA4 25 // derecha 2

#define PWMA_Ch    0
#define PWMA_Res   8
#define PWMA_Freq  60

#define PWMB_Ch    1
#define PWMB_Res   8  //resolucion del pwm
#define PWMB_Freq  60


int pL = 12;   
int pR = 14;

float e_actual = 0;
float e_anterior = 0;

float Kp = 3.6;  //constantes de PID
float Ki = 2.1;    //kte integracion
float Kd = 0;      //kte derivativa

float integ , deriv;    //vaiables que ayudan a calcular el error integral y derivativo
float c;      //correccion

int uh = 1800;   //umbral de los sensores, por encima de este valor detecta la linea

int pwmL = 120;   //pwm inicial
int pwmR = 125;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);   
  
  pinMode(INA1, OUTPUT);
  pinMode(INA2, OUTPUT);
  pinMode(INA3, OUTPUT);
  pinMode(INA4, OUTPUT);    //salidas digitales
  pinMode(pR, OUTPUT);   //salidas pwm
  pinMode(pL, OUTPUT);

  digitalWrite(INA1,HIGH);   //se establecen las direcciones de las ruedas
  digitalWrite(INA2,LOW);
  digitalWrite(INA3,LOW);
  digitalWrite(INA4,HIGH);
  ledcAttachPin(pL, PWMA_Ch);     //configuramos el pwm del motor izquierdo
  ledcSetup(PWMA_Ch, PWMA_Freq, PWMA_Res);
  ledcAttachPin(pR, PWMB_Ch);     //configuramos el pwm del motor derecho
  ledcSetup(PWMB_Ch, PWMB_Freq, PWMB_Res);
  
}

void loop() {
  
  int cent = analogRead(A0);   //hacemos la lectura de los sensores 
  int izq1 = analogRead(A3);   
  int der1 = analogRead(A6);
  int izq2 = analogRead(A7);
  int der2 = analogRead(A4);
  
//  Serial.print(izq2);
//  Serial.print("  ");
//  Serial.print(izq1);
//  Serial.print("  ");
//  Serial.print(cent);
//  Serial.print("  ");
//  Serial.print(der1);
//  Serial.print("  ");
//  Serial.println(der2);
//  Serial.println(c);
  
  
  if(cent>uh)
    e_actual = 0;
     
  if (der1>uh)
    e_actual = 5;    //error igual a 5 unidades, 
    
  if (der2>uh)
    e_actual = 16;
       
  if (izq1>uh)
    e_actual = -5;

  if (izq2>uh)
    e_actual = -16;

  integ = e_anterior + e_actual;      //calculamos error integral
  
  deriv = e_actual - e_anterior;     //calculamos error derivativo
  
  c = Kp*e_actual + Ki*integ + Kd*deriv;   //correccion total
  
  if(c>140) 
    c=140;   //si el error es mayor a 140 se lo limita a 140 para evitar problemas de desborde 
  if(c<-140)
    c=-140;  //si el error es menor de -140 se limita a 140 
  
  pwmL = 105 + c;
  pwmR = 110 - c;


  Serial.print(pwmL);
  Serial.print("  ");
  Serial.println(pwmR);
  
  if(pwmL >= 240)   //se limita el pwm a 240
    pwmL = 240;
  
  if(pwmL <= 10){   //si el pwm es menor que 10 la llanta izq debe desacelerar o frenar
    pwmL = 250;
    digitalWrite(INA2,HIGH);   //frena la llanta izquierda
  }
  else
    digitalWrite(INA2,LOW);   //sino deje la rueda en marcha
    
  if(pwmR >= 240)
    pwmR = 240;

  if(pwmR <= 10){            //si el pwm es menor a 10 la llanta debe frenar
    pwmR = 250;
    digitalWrite(INA3,HIGH);  //frena la llanta derecha
  }
  else
    digitalWrite(INA3,LOW);   //sino deje la llanta en marcha
  
  ledcWrite(PWMA_Ch, pwmL); // se entrega el pwm al puente H
  ledcWrite(PWMB_Ch, pwmR); // 
  
  e_anterior = e_actual;   //en el siguiente ciclo el error anterior debe ser el actual

  
}
