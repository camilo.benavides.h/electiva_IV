#include <Arduino.h>
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
/* Definiciones manejo motores */
#define INA1 26 //izquierda 1
#define INA2 27 // izquierda 2
#define INA3 14 // derecha 1
#define INA4 12 // derecha 2

#define PWMA_GPIO  2 // Derecha 
#define PWMA_Ch    0  //resolucion del pwm
#define PWMA_Res   8
#define PWMA_Freq  60
 
#define PWMB_GPIO  4 // Izquierda 
#define PWMB_Ch    1
#define PWMB_Res   8  //resolucion del pwm
#define PWMB_Freq  60

/* Pines para manejo de encoders */
#define ENC_IZQ 32
#define ENC_DER 34


/* Constante nro de orificios encoder */
#define NHOLES 20

#define TSAMPLE 1000


/* Definicion de variables Globales */
uint32_t u32gIzqTics = 0;
uint32_t u32gDerTics = 0;
uint32_t u32gTActual = 0;
uint32_t u32gTAnterior = 0;
uint32_t u32gTicsACT_L = 0;
uint32_t u32gTicksANT_L = 0;
uint32_t u32gTicsACT_R = 0;
uint32_t u32gTicksANT_R = 0;
uint32_t t_inicialIzq = 0;
uint32_t t_refIzq = 15;
uint32_t t_inicialDer = 0;
uint32_t t_refDer = 15;
uint32_t u32gDisCen = 0;




/*Variables Distancia Lineal*/
int r = 3.4;
int dm = 6.6;
int L = 14; 
int n = 20;
float tetha = 0;


int disTickDer = 10;
int tDeracT = 0; 
int tDeranT = 0;

int disTickIzq = 6;
int tIzqacT = 0; 
int tIzqanT = 0;

float Dr=0;
float Dl=0;
float Dc=0;
float DcAnt=0;

float x=0;
float y=0;
float pi=0;

unsigned long tAct = 0;
unsigned long tAnt = 0;
unsigned long tActT = 0;
 
int NVUELTAS = 3;

int giroIzq=0,giroDer=0;

void odometria (void);

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32t"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  pinMode(INA1, OUTPUT);
  pinMode(INA2, OUTPUT);
  pinMode(INA3, OUTPUT);
  pinMode(INA4, OUTPUT);
  pinMode(ENC_IZQ,INPUT);
  pinMode(ENC_DER,INPUT);

  delay(4000);
  /* Interrupciones para lectura de encoder */  
  attachInterrupt(digitalPinToInterrupt(ENC_IZQ), isrEncoderIzq, RISING);
  attachInterrupt(digitalPinToInterrupt(ENC_DER), isrEncoderDer, RISING);
  
  /*Test de un motor */
  digitalWrite(INA2, LOW);
  digitalWrite(INA3, LOW);
  
  ledcAttachPin(INA1, PWMA_Ch);     //configuramos el pwm del motor izquierdo
  ledcSetup(PWMA_Ch, PWMA_Freq, PWMA_Res);
  ledcAttachPin(INA4, PWMB_Ch);     //configuramos el pwm del motor derecho
  ledcSetup(PWMB_Ch, PWMB_Freq, PWMB_Res);
int pwm = 100;
int pwm9 = 110;
  
  ledcWrite(PWMA_Ch, pwm); // derecha 
  ledcWrite(PWMB_Ch, pwm9); // izquierda
  
}
void loop() {

  giroIzq = (u32gIzqTics/n);
  giroDer = (u32gDerTics/n);
  
  if (giroDer > NVUELTAS) {
    ledcWrite(PWMA_Ch, 0);    // derecha 
    Serial.println("NticksDer: " + String(u32gDerTics));
    u32gDerTics = 0;
     }

  //Serial.println("NticksDer: " + String(u32gDerTics));
  
 if (giroIzq > NVUELTAS) {
    ledcWrite(PWMB_Ch, 0);    // izquierda
    Serial.println("NticksIzq: " + String(u32gIzqTics));
    u32gIzqTics = 0;
    }
  
  //Serial.println("NticksDer: " + String(u32gIzqTics));
  
  
  tAct = millis();
  if((tAct - tAnt) > TSAMPLE) //toma muestra despues de Tsample milisegundos
  {
    odometria();
    Serial.print(x);
    Serial.print(" ; ");
    Serial.print(y);
    Serial.print(" ; ");
    Serial.println(pi);
    Serial.print(Dc);
    Serial.println(" Cm/Seg");
    Dc = 0;
    tAnt = tAct;
    Serial.println("NvueltasDer: " + String(giroDer));
    Serial.println("NvueltasIzq: " + String(giroIzq));
    u32gTicsACT_R = 0;
    u32gTicsACT_L = 0;
  }
}  


    
    /*IZQUIERDA*/
//    u32gTicsACT_L = u32gIzqTics;
//    uint32_t u32gIzqTics = u32gTicsACT_L - u32gTicksANT_L;
//    u32gTicksANT_L = u32gTicsACT_L;
//    uint32_t u32gDisL = (2 * pi * r * u32gTicsACT_L) / NHOLES;

    /*DERECHA*/
//    u32TicsACT_R = u32gDerTics;
//    uint32_t u32gDerTics = u32TicsACT_R - u32gTicksANT_R;
//    u32gTicksANT_R = u32TicsACT_R;
//    uint32_t u32gDisR = (2 * pi * r * u32TicsACT_R) / NHOLES;
    
 //    /* Estimacion de RPM */
//     uint16_t u16RpmL = (60 * u32gIzqTics)/ NHOLES;
//     uint16_t u16RpmD = (60 * u32gDerTics)/ NHOLES;
//     Serial.println("RpmL: " + String (u16RpmL));
//     Serial.println("RpmD: " + String (u16RpmD));
//     Serial.println("DirL:" + String(u32gDisL));
//     Serial.println("DirR:" + String(u32gDisR));

  

 /********************************************************************
 *  Interrupcion que se genera cada flanco de subida del encoder
 *  Ntick = = una vuelta del encoder. 
 * 
 *******************************************************************/
void isrEncoderIzq(void ){
  if((millis()-tActT)>11){
    u32gIzqTics++;
    u32gTicsACT_L++;
    tActT=millis();
  }
}

void isrEncoderDer(void ){
    u32gDerTics++;
    u32gTicsACT_R++;
}

/*odometria*/
void odometria(void){
  //dTickR = tRacT - tRanT; 
  disTickDer = (int) u32gTicsACT_R;
  Dr =  3.14159265 * dm * (disTickDer/(double)n);     
  
  //dTickL = tLacT - tLanT;
  disTickIzq = (int) u32gTicsACT_L; 
  Dl =  3.14159265 * dm * (disTickIzq/(double)n);
  
  Dc = (Dr + Dl)/2;
  x = x + Dc*cos(pi);             // posición del punto X actual
  y = y + Dc*sin(pi);             // posición del punto Y actual
  pi = pi + ((Dr - Dl)/L);        // posición Angular actua-l  
  pi = atan2(sin(pi),cos(pi));    //transformación de la posición angular entre -PI y PI 
  
  //tRacT = tRanT;    
  //tLacT = tLanT;
  
      SerialBT.print(x);
      SerialBT.print(";");
      SerialBT.print(y);
      SerialBT.print(";");
      SerialBT.println(0);
     
}
